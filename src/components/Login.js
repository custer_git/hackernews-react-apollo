import React, { Component } from 'react'
import { GC_USER_ID, GC_AUTH_TOKEN } from '../constants'
import { gql, graphql, compose } from 'react-apollo'

class Login extends Component {
    state = {
        login: true, // 在 login 和 signup 之间切换
        email: '',
        password: '',
        name: ''
    }

    render(){
        return(
            <div>
                <h4 className='mv3'>{this.state.login ? 'Login' : 'Sign Up'}</h4>
                <div className='flex flex-column'>
                    {!this.state.login &&
                    <input 
                    value={this.state.name}
                    onChange={(e) => this.setState({ name: e.target.value })}
                    type="text"
                    placeholder='用户名'
                    />}
                    <input 
                    value={this.state.email}
                    onChange={(e) => this.setState({ email: e.target.value })}
                    type="text"
                    placeholder='电子邮箱地址'
                    />
                    <input 
                    value={this.state.password}
                    onChange={(e) => this.setState({ password: e.target.value })}
                    type="password"
                    placeholder='密码'
                    />
                </div>
                <div className='flex mt3'>
                    <div
                        className='pointer mr2 button'
                        onClick={() => this._confirm()}
                    >
                        {this.state.login ? '登录' : '创建账户'}
                    </div>
                    <div
                        className='pointer button'
                        onClick={() => this.setState({ login: !this.state.login })}
                    >
                        {this.state.login ? '需要创建一个账户？' : '已经拥有账户'}
                    </div>
                </div>
            </div>
        )
    }

    _confirm = async () => {
        const { name, email, password } = this.state
        if (this.state.login) {
            const result = await this.props.signinUserMutation({
                variables: {
                    email,
                    password
                }
            })
            const id = result.data.signinUser.user.id
            const token = result.data.signinUser.token
            this._saveUserData(id, token)
        }else{
            const result = await this.props.createUserMutation({
                variables: {
                    name,
                    email,
                    password
                }
            })
            const id = result.data.signinUser.user.id
            const token = result.data.signinUser.token
            this._saveUserData(id, token)
        }
        this.props.history.push(`/`)
    }
    _saveUserData = (id, token) => {
        localStorage.setItem(GC_USER_ID, id)
        localStorage.setItem(GC_AUTH_TOKEN, token)
    }

}

const CREATE_USER_MUTATION = gql`
    mutation CreateUserMutation($name: String!, $email: String!, $password: String!) {
        createUser(
            name: $name,
            authProvider: {
                email:{
                    email: $email,
                    password: $password
                }
            }
        ){
            id
        }

        signinUser(email:{
            email: $email,
            password: $password
        }){
            token
            user {
                id
            }
        }
    }
`

const SIGNIN_USER_MUTATION = gql`
    mutation SigninUserMutation($email: String!, $password: String!) {
        signinUser(email:{
            email: $email,
            password: $password
        }) {
            token
            user{
                id
            }
        }
    }
`

export default compose(
    graphql(CREATE_USER_MUTATION, { name: 'createUserMutation' }),
    graphql(SIGNIN_USER_MUTATION, { name: 'signinUserMutation' }),
)(Login)