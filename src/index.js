import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import './styles/index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
// 导入需要的模块
import { ApolloProvider, createNetworkInterface, ApolloClient } from 'react-apollo'
import { GC_AUTH_TOKEN } from './constants'

/**
 * Apollo Client有一个可插拔的网络接口层，可以配置网络接口 使用 createNetworkInterface
 * 修改GraphQL服务器的URL，创建自定义的NetworkInterface
 * 
 * Query batching：当多个请求在一个特定的时间间隔内产生时（比如：100毫秒内）
 * Apollo会把多个查询组合成一个请求，
 * 比如在渲染一个包含导航条，边栏，内容等带有GraphQL查询的组件时
 * 使用Query batching,要传递BatchedNetworkInterface给ApolloClient构造函数
 */
const networkInterface = createNetworkInterface({
    uri: 'https://api.graph.cool/simple/v1/cj6ks1dgw17pl0165izaj3fyr'
})

networkInterface.use([{
    applyMiddleware(req, next){
        if(!req.options.headers){
            req.options.headers = {}
        }
        const token = localStorage.getItem(GC_AUTH_TOKEN)
        req.options.headers.authorization = token ? `Bearer ${ token }` : null
        next()
    }
}])
/**
 * 实例化ApolloClient - 默认情况客户端会发送到相同主机名（域名）下的/graphql端点
 * 查询去除重复(Query deduplication)
 * 查询去除重复可以减少发送到服务器的查询数量，默认关闭
 * 通过queryDeduplication选项传递给ApolloClient构造函数开启
 * 查询去重在多个组件显示相同数据的时候非常有用，避免从服务器多次获取相同的数据
 */
const client = new ApolloClient({
    networkInterface
})
/* 挂载组件 - 要连接客户端到React组件树，要确保ApolloProvider作为一个容器去包裹
其他的需要访问GraphQL服务器数据的React组件 */
ReactDOM.render(
    <BrowserRouter>
        <ApolloProvider client={client}>
            <App />
        </ApolloProvider>
    </BrowserRouter>, document.getElementById('root')
)

registerServiceWorker()
